package Controller;

import Model.Client;
import Model.Input;
import Model.Queue;
import Model.RCG;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Thread.sleep;

public class SimulationManager implements Runnable{
    private String inputFileName;
    private static FileWriter myWriter;
    public SimulationManager(String inputFileName, String outputFileName) {
        this.inputFileName=inputFileName;
        try {
            File myObj = new File(outputFileName);
            myObj.createNewFile();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        try {
            myWriter = new FileWriter(outputFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void run(){
        Input getInputData = new Input(inputFileName);
        int numberOfClients = getInputData.getNumberOfClients();
        int numberOfQueues = getInputData.getNumberOfQueues();
        int maxSimulationTime = getInputData.getMaxSimulationTime();
        int minArrivalTime = getInputData.getMinArrivalTime();
        int maxArrivalTime = getInputData.getMaxArrivalTime();
        int minServiceTime = getInputData.getMinServiceTime();
        int maxServiceTime = getInputData.getMaxServiceTime();
        int currentTime = 0;
        //generate clients
        List<Client> clients;
        RCG randomClientGenerator = new RCG();
        clients = randomClientGenerator.generate(numberOfClients,minArrivalTime,maxArrivalTime,minServiceTime,maxServiceTime);
        Collections.sort(clients);
        //generate queues
        List<Queue> queues;
        queues = generateQueues(numberOfQueues);
        Queue q;
        while(currentTime <= maxSimulationTime){
            while (clients.size() > 0 && clients.get(0).getArrivalTime() == currentTime) {
                    q = getQueue(queues);
                    q.add(clients.get(0));
                    clients.remove(0);
            }
            try {
                printLog(currentTime,queues,clients);
            } catch (IOException e) {
                e.printStackTrace();
            }
            decreaseServiceTime(queues);
            currentTime++;
            try{
                sleep(100);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        double avgWaitTime=computeAvgWaitTime(queues,numberOfClients);
        try {
            printAvgWaitTime(avgWaitTime);
        } catch (IOException e) {
            e.printStackTrace();
        }

        gentlyKillThreads(queues);

    }

    public static List<Queue> generateQueues(int noQueues)
    {
        List<Queue> qs = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < noQueues; i++) {
            qs.add(new Queue(i,50));
            (new Thread(qs.get(i))).start();
        }
        return qs;
    }

    public static Queue getQueue(List<Queue> queues)
    {
        Queue q = null;
        int minAvgWaitingTime = Integer.MAX_VALUE;
        for(Queue queue: queues) {
            if (minAvgWaitingTime > queue.getAvgWaitTime()) {
                minAvgWaitingTime = queue.getAvgWaitTime();
                q = queue;
            }
        }
        return q;
    }

    public static void decreaseServiceTime(List<Queue> queues){
        for(Queue queue: queues) {
            if(!queue.getQueue().isEmpty()){
                queue.getQueue().peek().setServiceTime(queue.getQueue().peek().getServiceTime()-1);
                queue.setAvgWaitTime(queue.getAvgWaitTime()-1);
            }
        }

    }

    public static void printLog(int currentTime, List<Queue> queues, List<Client> clients) throws IOException {
        myWriter.write("Time: "+currentTime+"\n");
        myWriter.write("Waiting clients: ");
        for(Client c:clients){
            myWriter.write(c.toString());
        }
        myWriter.write(" \n");
        for(Queue q:queues){
            myWriter.write(q.toString()+"\n");
        }
        myWriter.write(" \n");
    }

    public static double computeAvgWaitTime(List<Queue> queues,int nbOfClients){
        double totalWaitTime=0;
        for(Queue q:queues){
            totalWaitTime+=q.getTotalClientWaitTime().get();
        }
        return totalWaitTime/nbOfClients;
    }


    public static void printAvgWaitTime(double waitTime) throws IOException {
        myWriter.write("\nAverage waiting time: "+waitTime);
        try {
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static  void gentlyKillThreads(List<Queue> queues) {
        for(Queue q:queues){
            q.stop();
        }
    }
}
