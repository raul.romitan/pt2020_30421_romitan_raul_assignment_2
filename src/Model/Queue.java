package Model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.sleep;

public class Queue implements Runnable{
    private AtomicInteger id=new AtomicInteger();
    private BlockingQueue<Client> queue;
    private AtomicInteger totalClientWaitTime= new AtomicInteger();
    private AtomicInteger avgWaitTime=new AtomicInteger();
    private boolean running;

    public Queue(int id, int size){
        this.id.set(id);
        this.queue= new ArrayBlockingQueue<>(size);
        this.totalClientWaitTime.set(0);
        this.avgWaitTime.set(0);
        this.running=true;
    }

    public void add(Client c){
        queue.offer(c);
        this.avgWaitTime.set(avgWaitTime.get()+c.getServiceTime());
        this.totalClientWaitTime.set(totalClientWaitTime.get()+avgWaitTime.get());
        synchronized (this){
            notify();
        }
    }

    @Override
    public void run(){
        while(running) {

            if(!queue.isEmpty()){
                Client c = queue.peek();
                try{
                    int timeToWait=c.getServiceTime();
                    sleep( timeToWait * 100);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                queue.remove();
            }else{
                try{
                    synchronized (this){
                        wait();
                    }
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }

        }
    }

    public int getAvgWaitTime(){
        return this.avgWaitTime.get();
    }

    public AtomicInteger getTotalClientWaitTime() {
        return totalClientWaitTime;
    }

    public void setAvgWaitTime(int avgWaitTime) {
        this.avgWaitTime.set(avgWaitTime);
    }

    @Override
    public String toString() {
        if(queue.isEmpty())
            return "Queue "+id+": closed;";
        else
            return "Queue " + id + ": " + queue +";";
    }

    public BlockingQueue<Client> getQueue() {
        return queue;
    }

    public void stop(){
        synchronized (this){
            notify();
        }
        this.running=false;
    }
}
