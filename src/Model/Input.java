package Model;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class Input {
    private static int numberOfClients;
    private static int numberOfQueues;
    private static int maxSimulationTime;
    private static int minArrivalTime;
    private static int maxArrivalTime;
    private static int minServiceTime;
    private static int maxServiceTime;
    public Input(String inputFileName){
        Scanner in = null;
        try {
            in = new Scanner(new FileReader(inputFileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        assert in != null;
        numberOfClients=Integer.parseInt(in.next());
        numberOfQueues=Integer.parseInt(in.next());
        maxSimulationTime=Integer.parseInt(in.next());
        String readLine;
        readLine=in.next();
        String []times;
        times=readLine.split(",");
        minArrivalTime=Integer.parseInt(times[0]);
        maxArrivalTime=Integer.parseInt(times[1]);
        readLine=in.next();
        times=readLine.split(",");
        minServiceTime=Integer.parseInt(times[0]);
        maxServiceTime=Integer.parseInt(times[1]);
        in.close();
    }

    public int getNumberOfQueues() {
        return numberOfQueues;
    }

    public int getMaxSimulationTime() {
        return maxSimulationTime;
    }

    public int getNumberOfClients() {
        return numberOfClients;
    }

    public int getMinArrivalTime() {
        return minArrivalTime;
    }

    public int getMaxArrivalTime() {
        return maxArrivalTime;
    }

    public int getMinServiceTime() {
        return minServiceTime;
    }

    public int getMaxServiceTime() {
        return maxServiceTime;
    }
}

