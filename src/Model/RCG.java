package Model;

import java.util.ArrayList;
import java.util.List;

public class RCG {
    public List<Client> generate(int numberOfClients, int minArrivalTime, int maxArrivalTime, int minServiceTime, int maxServiceTime) {
        maxArrivalTime+=1;
        maxServiceTime+=1;
        List<Client> clients = new ArrayList<>();
        for (int i = 0; i < numberOfClients; i++) {
            int randomArrivalTime = (int)(Math.random() * (maxArrivalTime-minArrivalTime)+minArrivalTime);
            int randomServiceTime = (int)(Math.random() * (maxServiceTime-minServiceTime)+minServiceTime);
            Client client = new Client(i+1, randomArrivalTime, randomServiceTime);
            clients.add(client);
        }
        return clients;
    }


}
